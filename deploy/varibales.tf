variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "moneycontrol-app-api"
}

variable "contact" {
  default = "3734489@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "moneycontrol-app-api-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "276063048928.dkr.ecr.us-east-1.amazonaws.com/moneycontrol-app-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "276063048928.dkr.ecr.us-east-1.amazonaws.com/moneycontrol-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "email_host_user" {
  description = "Email address for Django app"
}

variable "email_host_password" {
  description = "Email password for Django app"
}
